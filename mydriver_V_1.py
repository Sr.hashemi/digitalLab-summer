#mydriver.py
#ver_1

##### import packages #####
import serial
import sys		#for terminating the program immediately
import time


##### global variables ####
#rSpeed = 0
#lSpeed = 0



##### robot class #####
class autRobot:
	def __init__(self):
		#self.portfd = 0;        # File descriptor of robot which data in written to and has to be read from
		self.motorSpeedLeft = 0        # The number has to be set on the left motor input
		self.motorSpeedRight = 0   # The number has to be set on the right motor input
		self.motorShaftLeft = 0        # Left shaft encoder data
		self.motorShaftRight = 0   # Right shaft encoder data
		self.omegaL = 0.0      # Left angular velocity
		self.omegaR = 0.0      # Right angular velocity
		self.sonarRear = 0         # data of sonar number 0
		self.sonarRearL = 0            # data of sonar number 1
		self.sonarFrontL = 0           # data of sonar number 2
		self.sonarFront = 0            # data of sonar number 3
		self.sonarFrontR = 0           # data of sonar number 4
		self.sonarRearR = 0            # data of sonar number 5
		self.battery = 0.0     # battery life percentage
		self.reset = 0         # does robot need reseting?
		
		##### try to connect to serial port #####
		try:
			ser = serial.Serial("/dev/ttyUSB0",38400)   # try to connect to serial port
			self.portfd = ser
			print("Successfully connected to seial! (USB0)")
		except:
			try:
				ser = serial.Serial("/dev/ttyUSB1",38400)
				self.portfd = ser
				print("Successfully connected to serial! (USB1)")
			except:
				print("Sorry! Can NOT connect to the robot!")
				sys.exit() #terminates the program immediately



##### create robot object #####
robot = autRobot()


##### write motor speed in serial port #####
def writeData(rSpeed, lSpeed):
	robot.portfd.write(bytes("S %d %d\n"%(rSpeed, lSpeed), 'UTF-8'))


##### read date from serial port & assign  them to robot  #####
def readData():
	data = []
	for count in range(0,9):
		tmp = robot.portfd.readline().decode('UTF-8')         
		if(count < 8):
			data.append(int(tmp))
			#print ("The %d's data is %s"%(count+1, data[count]))
		else:
			robot.battery = float(tmp)
			#print(robot.battery)

	robot.motorShaftLeft = data[0]
	robot.motorShaftRight = data[1]
	robot.sonarRear = data[2]
	robot.sonarRearL = data[3]
	robot.sonarFrontL = data[4]
	robot.sonarFront = data[5]
	robot.sonarFrontR = data[6]
	robot.sonarRearR = data[7]  

##### print robot data in terminal #####
def printData():
	#pfd = "Port File Descriptor: %s"% (robot.portfd)
	mspl = "Left Motor Speed: %d"% (robot.motorSpeedLeft)
	mspr = "Right Motor Speed: %d"% (robot.motorSpeedRight)
	mshl = "Left Shaft Encoder Data: %d"% (robot.motorShaftLeft)
	mshr = "Right Shaft Encoder Data: %d"% (robot.motorShaftRight)
	sr = "Rear Sonar Data: %d"% (robot.sonarRear)
	srl = "Rear-Left Sonar Data: %d"% (robot.sonarRearL)
	sfl = "Front-Left Sonar Data: %d"% (robot.sonarFrontL)
	sf = "Front Sonar Data: %d"% (robot.sonarFront)
	sfr = "Front-Right Sonar Data: %d"% (robot.sonarFrontR);
	srr = "Rear-Right Sonar Data: %d"% (robot.sonarRearR);
	bt = "Battery Voltage: %f"% (robot.battery);

	print("\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n"%(mspl, mspr, mshl, mshr, sr, srl, sfl, sf, sfr, srr, bt))


for i in range(0,150,20):
	rSpeed = lSpeed = i
	print(i)
	writeData(rSpeed, lSpeed)
	readData()
	printData()
	time.sleep(1)