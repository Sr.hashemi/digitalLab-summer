# Digital Lab Codes
By SReza Hashemi (SR.Hashemirad@yahoo.com)

## [mydriver_V_1](mydriver_V_1.py)
Just set motor speed!


## [mydriver_V_2](mydriver_V_2.py)
Change motor speed by keyboard!


## [mydriver_V_3](mydriver_V_3.py)
Move robot with keyboard!


## [mydriver_V_4](mydriver_V_4.py)
Control Robot via webpage! (method1)


## [mydriver_V_5](mydriver_V_5.py)
Control Robot via webpage! (method2)

- [ ] task one
- [ ] task two
- [x] task three
- [ ] task four